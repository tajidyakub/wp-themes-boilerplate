<?php
/**
 * Template's functions file.
 * Automatically availble in template file;
 *
 * @package boilerplate
 * @since 1.0.0
 */

// TODO: Web and App Icons + Manifest file

// Themes setup hooked to wordpress
add_action('after_setup_theme', boilerplate_setup);

// Themes setup function
// - Theme's support
// - Post type support
// - Post thumbnails
// - Thumbail sizes
// - Register menus
// - Custom mod for logos
// - Internationalization (textdomain)

function boilerplate_setup() {
    add_theme_support( 'title-tag' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'post-thumbnails' );

    // Custom background feature
    add_theme_support( 'custom-background', apply_filters( 'boilerplate_custom_bg_args', array(
        'default-color' => 'ffffff',
        'default-image' => '',
    ) ) );

    // Custom Logo
    add_theme_support( 'custom-logo', array(
        'height'      => 32,
        'width'       => 250,
        'flex-width'  => true,
        'flex-height' => true,
    ) );

    // Switch default core markup for search form, comment form, and comments
    // to output valid HTML5.
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );
    // Register Nav Menu Locations
    register_nav_menus( array(
        'primary'   => __( 'Primary Menu', 'blackwp' ),
        'secondary' => __( 'Secondary Menu', 'blackwp' ),
        'footer' => __( 'Footer Menu', 'blackwp' )
    ) );

    // Internationalization
    load_theme_textdomain( 'blackwp', get_template_directory() . '/langs' );

    // Add image sizes
    add_image_size( 'large', 1280, 768, true );
    add_image_size( 'medium', 1024, 576, true );
    add_image_size( 'thumbnail', 240, 135, true );
}


// Register image sizes tobe available in Add Media modal
add_filter( 'image_size_names_choose', 'boilerplate_custom_sizes' );

// Image Sizes function callback
function boilerplate_custom_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'large' => __( 'Large Size' ),
        'medium' => __( 'Medium Size' ),
        'thumbnail' => __( 'Thumbnail Size' ),
    ) );
}

add_action( 'widgets_init', 'kickstart_widgets_init' );

// Register widget area.
function boilerplate_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'boilerplate' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'boilerplate' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '<small><i class="fab fa-hotjar"></i></small></h2>',
  ) );
  register_sidebar( array(
		'name'          => esc_html__( 'Footer', 'boilerplate' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'boilerplate' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
  ) );
}

add_action( 'wp_enqueue_scripts', 'boilerplate_enqueue' );
// Enqueue Styles and Scripts
function boilerplate_enqueue() {
    // TODO:: fontawesome styles
    wp_enqueue_style( 'bp/css/style', get_stylesheet_uri() , array(), filemtime( get_template_directory() . '/style.css'), 'all');
    wp_enqueue_script( 'bp/js/web', get_template_directory_uri() . '/js/web.js', array(), filemtime( get_template_directory() . '/js/web.js'), true );
}

// Custom fields
// TODO: better placement will be in site plugin.

add_action('the_subtitle', 'the_subtitle');

function get_the_subtitle( $post_id ) {
    $key     = 'subtitle';
    $single  = true;
    return get_post_meta($post_id, $key, $single);
}
function the_subtitle( $post_id ) {
    echo get_the_subtitle( $post_id );
}

// Web headers.
// Display custom logo is already defined in the customizer.

add_action('the_web_header', 'the_web_header');

function get_the_web_header() {
    $logo_id   = get_theme_mod( 'custom_logo' );
    $logo      = wp_get_attachment_image_src( $logo_id , 'full' );
    $web_title = get_bloginfo('name');
    if (has_custom_logo) {
        return '<a href="/" title="'. __( 'Back to home', 'boilerplate' ) .'"><img src=". $logo[0] ." alt="'. $web_title .'" /></a>';
    } else {
        return '<h1><a href="/" title="'. __( 'Back to home', 'boilerplate' ) .'">'. $web_title .'</h1></a>';
    }
}

function the_web_header() {
    echo get_the_web_header();
}

<?php
/**
 * Template index. will be displayed in
 * home url.
 *
 * @package boilerplate
 * @since 1.0.0
 */

// get the header
get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col text-center py-5">
            <h1>Welcome.</h1>
            <p>Look for the Theme's development files in the src folder.</p>
        </div>
    </div>
</div>

<?php
// get the footer
get_footer(); ?>

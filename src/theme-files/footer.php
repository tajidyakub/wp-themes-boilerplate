<?php
/**
 * Template's footer file.
 * Included from the main file using get_footer();
 *
 * @package boilerplate
 * @since 1.0.0
 */
?>
        <!-- Page footer -->
        <footer class="bg-dark text-white">
            <div class="container">
                <div class="row">
                    <div class="col pt-5 pb-4">
                        <h4>Footer</h4>
                    </div>
                </div>
            </div>
        </footer>

        <?php
        // wp_footer hook call before closing html body.
        wp_footer();?>
    </body>
</html>

<?php
/**
 * Template's header file.
 * Included from the main file using get_header();
 *
 * @package boilerplate
 * @since 1.0.0
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <!-- HTML Head simpify, title, icons, styles will be loaded from functions -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php
            // wp_head(), html head hook call before </head>
            wp_head(); ?>
    </head>
    <body>
    <?php
        // Generic top navigation.
        get_template_part('parts/navigation', 'top')?>


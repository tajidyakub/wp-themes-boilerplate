/**
 * The files join together Twitter Bootstrap
 * required modules into one file for simplicity while developing.
 */

let bootstrap = require('bootstrap');
let jQuery = require('jquery');
let popperjs = require('popper.js');

window.$ = $ = jQuery

$(document).on('ready', function ($) {
  console.log('Hello from boilerplate.')
})

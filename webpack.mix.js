let mix = require('laravel-mix');
mix.sass('src/scss/style.scss', 'src/theme-files/style.css')
  .options({
    processCss: false
  })
  .js('src/scripts/web.js', 'src/theme-files/js/web.js')

const config = {

  themes: {
    slug: 'boilerplate',
    version: '1.0.0'
  },

  remote: {
    conn: 'user@ip.address',
    themes: '/var/www/domain.com/htdocs/wp-content/themes/boilerplate',
    syncTo: conn + ':' + themes
  },

  local: {
    themes: './webroot/wp-content/themes/boilerplate', // Important not to use trailing slash '/'
    dev: './src/theme-files/'
  },

  sync: {
    exclude: ["*.DS_Store", ".git*", "*.scss"]
  }
}

module.exports = config;

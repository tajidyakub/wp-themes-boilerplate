/**
 * Grunt tasks definition file.
 *
 * Adjust configurations in the given config file
 * and rename accodingly tobe required from this file.
 */
const _config = require('./_config/_config');

module.exports = function (grunt) {
  grunt.initConfig({
    // Deployment through rsync, will synchronize local dev folder with
    // Web root, either local or remote
    rsync: {
      options: {
        args: ["--verbose"],
        exclude: _config.sync.exclude,
        recursive: true
      },
      local: {
        options: {
          src: _config.local.dev,
          dest: _config.local.themes,
          delete: true
        }
      },
      remote: {
        options: {
          src: _config.local.dev,
          dest: _config.remote.syncTo,
          delete: true
        }
      }
    },
    compress: {
      themes: {
        options: {
          archive: 'released/' + _config.themes.slug + '-' + _config.themes.version + '.zip'
        },
        expand: true,
        cwd: 'src/' + theme-files + '/',
        src: ['**/*'],
        dest: '/'
      }
    }
  })

  // Next one would load plugins
  grunt.loadNpmTasks('grunt-rsync')
  grunt.loadNpmTasks('grunt-contrib-compress')
  // grunt.loadNpmTasks('grunt-simple-git')
  // Here is where we would define our task
  grunt.registerTask('sync', 'Deploy to local development web server' , ['rsync:local'])
  grunt.registerTask('deploy', 'Deploy to remote web server' , ['rsync:remote'])
  // grunt.registerTask('remote-sync', 'Deploy plugin forlder to dist plugin path', ['rsync:deployPlugin'])
  grunt.registerTask('release', 'Compress files in dist/', ['compress:themes'])
};
